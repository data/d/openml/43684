# OpenML dataset: COVID-19-Stats-and-Mobility-Trends

https://www.openml.org/d/43684

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

COVID-19 Stats  Trends
Context
This dataset seeks to provide insights into what has changed due to policies aimed at combating COVID-19 and evaluate the changes in community activities and its relation to reduced confirmed cases of COVID-19. The reports chart movement trends, compared to an expected baseline, over time (from 2020/02/15 to 2020/02/05) by geography (across 133 countries), as well as some other stats about the country that might help explain the evolution of the disease.
Content

Grocery  Pharmacy: Mobility trends for places like grocery markets, food warehouses, farmers' markets, specialty food shops, drug stores, and pharmacies.
Parks: Mobility trends for places like national parks, public beaches, marinas, dog parks, plazas, and public gardens.
Residential: Mobility trends for places of residence.
Retail  Recreation: Mobility trends for places like restaurants, cafes, shopping centers, theme parks, museums, libraries, and movie theaters.
Transit stations: Mobility trends for places like public transport hubs such as subway, bus, and train stations.
Workplaces: Mobility trends for places of work.
Total Cases: Total number of people infected with the SARS-CoV-2.
Fatalities: Total number of deaths caused by CoV-19.
Government Response Stringency Index: Additive score of nine indicators of government response to CoV-19: School closures, workplace closures, cancellation of public events, public information campaigns, stay at home policies, restrictions on internal movement, international travel controls, testing policy, and contact tracing.
COVID-19 Testing: Total number of tests performed.
Total Vaccinations: Total number of shots given. 
Total People Vaccinated: Total number of people given a shot.
Total People Fully Vaccinated: Total number of people fully vaccinated (might require two shots of some vaccines).
Population: Total number of inhabitants.
Population Density per km2: Number of human inhabitants per square kilometer.
Health System Index: Overall performance of the health system.
Human Development Index (HDI): Summary index based on life expectancy at birth, expected years of schooling for children and mean years of schooling for adults, and GNI per capita.
GDP (PPP) per capita: Gross Domestic Product (GDP) per capita based on Purchasing Power Parity (PPP), taking into account the relative cost of local goods, services and inflation rates of the country, rather than using international market exchange rates, which may distort the real differences in per capita income.
Elderly Population (percentage): Percentage of the population above the age of 65 years old.

References  Acknowledgements
Bing COVID-19 data. Available at: https://github.com/microsoft/Bing-COVID-19-Data
COVID-19 Community Mobility Report. Available at: https://www.google.com/covid19/mobility/
COVID-19: Government Response Stringency Index. Available at: https://ourworldindata.org/grapher/covid-stringency-index
Coronavirus (COVID-19) Testing. Available at: https://github.com/owid/covid-19-data/blob/master/public/data/testing/covid-testing-all-observations.csv
Coronavirus (COVID-19) Vaccination. Available at: https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/vaccinations.csv
List of countries and dependencies by population. Available at: https://www.kaggle.com/tanuprabhu/population-by-country-2020
List of countries and dependencies by population density. Available at: https://www.kaggle.com/tanuprabhu/population-by-country-2020
List of countries by Human Development Index. Available at: http://hdr.undp.org/en/data
Measuring Overall Health System Performance. Available at: https://www.who.int/healthinfo/paper30.pdf?ua=1
List of countries by GDP (PPP) per capita. Available at: https://data.worldbank.org/indicator/NY.GDP.PCAP.PP.CD
List of countries by age structure (65+). Available at: https://data.worldbank.org/indicator/SP.POP.65UP.TO.ZS
Authors

Diogo Silva, up201706892fe.up.pt

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43684) of an [OpenML dataset](https://www.openml.org/d/43684). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43684/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43684/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43684/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

